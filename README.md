Matt has defended virtually every type of criminal case and enjoys an outstanding success rate. Matt’s methods are aggressive and direct. Due to his experience and background, he can tailor the best defense for each client’s needs.

Address: 3355 W Alabama St, Suite 444, Houston, TX 77098, USA

Phone: 713-224-6666

Website: https://mattalfordlaw.com
